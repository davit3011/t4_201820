package view;

import java.util.Scanner;

import controller.Controller;
import model.data_structures.IDoublyLinkedList;
import model.vo.VOTrip;

public class DivvyTripsManagerView 
{
	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
		boolean fin=false;
		while(!fin)
		{
			printMenu();
			
			int option = sc.nextInt();
			
			switch(option)
			{
				case 1:
					Controller.loadTrips();;
					break;
					
				case 2:
					System.out.println("Ingrese el n�mero de viajes a mostrar");
					Integer n = Integer.parseInt(sc.next());
					Controller.getLastNTrips(n);
					break;
					
				case 3:
					Controller.ordenarMerge();
					break;
					
				case 4:
					Controller.ordenarQuickSort();
					break;
					
				case 5:	
					fin=true;
					sc.close();
					break;
			}
		}
	}

	private static void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 4----------------------");
		System.out.println("1. Crear colecci�n de viajes");
		System.out.println("2. Dar ultimos N viajes");
		System.out.println("3. Ordenar por Merge");
		System.out.println("4. Ordenar por Quick");
		System.out.println("5. Salir");
		System.out.println("Digite el n�mero de opci�n para ejecutar la tarea, luego presione enter: (Ej., 1):");
		
	}
}
