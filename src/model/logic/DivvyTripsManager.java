package model.logic;

import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;

import com.opencsv.CSVReader;

import api.IDivvyTripsManager;
import model.vo.VOBike;
import model.vo.VOTrip;
import ordenamientos.MergeSort;
import ordenamientos.QuickSort;
import model.data_structures.DoubleLinkedList;
import model.data_structures.DoubleLinkedList.ListIterator;
import model.data_structures.IDoublyLinkedList;
import model.data_structures.Nodo;
import model.data_structures.Queue;
import model.data_structures.Stack;

public class DivvyTripsManager implements IDivvyTripsManager {
	private DoubleLinkedList<VOTrip> listaViajes;
	private DoubleLinkedList<VOBike> listaEstaciones;
	private CSVReader reader;
	private CSVReader reader2;
	private Stack<VOTrip> pila;
	private Queue<VOTrip> cola;
	private Comparable<VOTrip>[] datos;
	private Stack<VOTrip> cuantosViajes;

	public DivvyTripsManager()
	{
		listaViajes=new DoubleLinkedList<>();
		listaEstaciones=new DoubleLinkedList<>();
		
	}

	


	public void loadTrips (String tripsFile) {
		String[] lineaLeer;
		int contador=0;
		try {
			reader = new CSVReader(new FileReader(tripsFile));
            System.out.println("hola");
			lineaLeer=reader.readNext();
			while((lineaLeer = reader.readNext())!=null)
			{	
					int id=Integer.parseInt(lineaLeer[0]);
					String startTime= lineaLeer[1];
					String endTime=lineaLeer[2];
					int bikeId=Integer.parseInt(lineaLeer[3]);
					int tripDuration=Integer.parseInt(lineaLeer[4]);
					int fromStationId=Integer.parseInt(lineaLeer[5]);
					String fromStationName=lineaLeer[6];
					int toStationId=Integer.parseInt(lineaLeer[7]);
					String toStationName=lineaLeer[8];
					String userType=lineaLeer[9];
					String gender=lineaLeer[10];
					String birth=lineaLeer[11];
					VOTrip station=new VOTrip(id, startTime, endTime, bikeId, tripDuration, fromStationId, fromStationName,toStationId, toStationName, userType, gender, birth );
					pila.push(station);
					System.out.println("" +  contador);
				datos = new Comparable[pila.size()];
				Stack<VOTrip> pilaTemp= new Stack<VOTrip>();
				datos= new Comparable[pila.size()];
				for (int i = 0; i < datos.length; i++) {
					VOTrip trip= pila.pop();
					datos[i] = trip;
					pilaTemp.push(trip);
				}			
				for(int j=0; j<datos.length; j++)
				{
					pila.push(pilaTemp.pop());
				}


		}
		}
		catch (Exception e) {
			System.out.println("" + e.getMessage());
		}
}
	// TODO Auto-generated method stub



	@Override
	public String  getLastNTrips(int n) throws Exception{
		if(n>1119814)
		{
			throw new Exception("No hay tantos Trips");
		}
		
		int contador =0;
		String viajes="";
		while(contador>= n)
		{
			cuantosViajes.push(pila.pop());
			contador--;
		}
		viajes = "Todav�a hay:" + pila.size() + "viajes";
		return viajes;
}

	@Override
	public IDoublyLinkedList<String> getLastNStations(int bicycleId, int n) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public VOTrip customerNumberN(int stationID, int n) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void ordenarMerge() 
	{
		long tiempo1 = System.currentTimeMillis();
		MergeSort.sort(datos);
		long b = System.currentTimeMillis();
		System.out.println("El tiempo que se demor� en cargar:" + datos.length + "datos fue:" + (b-tiempo1));
		
	}

	@Override
	public void ordenarQuick() {
		long a = System.currentTimeMillis();
		QuickSort.sort(datos);
		long b = System.currentTimeMillis();
		System.out.println("El tiempo que le tom� ordenar los" + datos.length + "datos fue: " +(b-a) );
		
	}
	public void medirTiempoMerge()
	{
		Comparable<VOTrip>[] a = new Comparable[200];
		for(int i =0; i< a.length; i++)
		{
			a[i]= pila.pop();
		}
		long tiempoA = System.currentTimeMillis();
		MergeSort.sort(a);
		long tiempoB = System.currentTimeMillis();
		System.out.println("El tiempo que se demor� en cargar los" + a.length + "elementos" + "fue de:" +(tiempoB-tiempoA));
		Comparable<VOTrip>[] b = new Comparable[1000];
		for(int i =0; i< b.length; i++)
		{
			b[i]= pila.pop();
		}
		long tiempoC = System.currentTimeMillis();
		MergeSort.sort(b);
		long tiempoD = System.currentTimeMillis();
		System.out.println("El tiempo que se demor� en cargar los" + b.length + "elementos" + "fue de:" +(tiempoD-tiempoC));
		
		Comparable<VOTrip>[] c = new Comparable[900000];
		for(int i =0; i< c.length; i++)
		{
			c[i]= pila.pop();
		}
		long tiempoE = System.currentTimeMillis();
		MergeSort.sort(c);
		long tiempoF = System.currentTimeMillis();
		System.out.println("El tiempo que se demor� en cargar los" + c.length + "elementos" + "fue de:" +(tiempoF-tiempoE));
		
		Comparable<VOTrip>[] d = new Comparable[10000];
		for(int i =0; i< d.length; i++)
		{
			d[i]= pila.pop();
		}
		long tiempoG = System.currentTimeMillis();
		MergeSort.sort(d);
		long tiempoH = System.currentTimeMillis();
		System.out.println("El tiempo que se demor� en cargar los" + d.length + "elementos" + "fue de:" +(tiempoH-tiempoG));
		
		
		
	}
	public void medirTiempoQuick()
	{
		Comparable<VOTrip>[] a = new Comparable[200];
		for(int i =0; i< a.length; i++)
		{
			a[i]= pila.pop();
		}
		long tiempoA = System.currentTimeMillis();
		QuickSort.sort(a);
		long tiempoB = System.currentTimeMillis();
		System.out.println("El tiempo que se demor� en cargar los" + a.length + "elementos" + "fue de:" +(tiempoB-tiempoA));
		Comparable<VOTrip>[] b = new Comparable[1000];
		for(int i =0; i< b.length; i++)
		{
			b[i]= pila.pop();
		}
		long tiempoC = System.currentTimeMillis();
		QuickSort.sort(b);
		long tiempoD = System.currentTimeMillis();
		System.out.println("El tiempo que se demor� en cargar los" + b.length + "elementos" + "fue de:" +(tiempoD-tiempoC));
		
		Comparable<VOTrip>[] c = new Comparable[900000];
		for(int i =0; i< c.length; i++)
		{
			c[i]= pila.pop();
		}
		long tiempoE = System.currentTimeMillis();
		QuickSort.sort(c);
		long tiempoF = System.currentTimeMillis();
		System.out.println("El tiempo que se demor� en cargar los" + c.length + "elementos" + "fue de:" +(tiempoF-tiempoE));
		
		Comparable<VOTrip>[] d = new Comparable[10000];
		for(int i =0; i< d.length; i++)
		{
			d[i]= pila.pop();
		}
		long tiempoG = System.currentTimeMillis();
		QuickSort.sort(d);
		long tiempoH = System.currentTimeMillis();
		System.out.println("El tiempo que se demor� en cargar los" + d.length + "elementos" + "fue de:" +(tiempoH-tiempoG));
		
	}




	@Override
	public void loadStations(String stationsFile) {
		// TODO Auto-generated method stub
		
	}
	

	//@Override
	//public VOTrip customerNumberN (int stationID, int n) {
		//VOTrip rta=null;
		//Iterator<VOTrip> iter = listaViajes.iterator();
		//while(iter.hasNext())
		//{
			//VOTrip actual = iter.next();
			//if(actual.id()==n && actual.darToStationId()==stationID)
			//{
				//rta=actual;

			//}
		//}
		//return rta;
	//}
	


}
