package model.vo;

/**
 * Representation of a Trip object
 */
public class VOTrip implements Comparable<VOTrip>
{
	private int trip_id;
	private String start_time;
	private String end_time;
	private int bikeid;
	private int tripduration;
	private int from_station_id;
	private String from_station_name;
	private int to_station_id;
	private String to_station_name;
	private String usertype;
	private String gender;
	private String birthyear;
	public VOTrip(int pTrip_id, String pStartT, String pEndT, int pbikeId, int ptripDuration, int fmsid,
			String fmsName, int tosId, String tosName, String pusertype, String pgender, String pBirthYear )
	{
		trip_id=pTrip_id;
		start_time=pStartT;
		end_time=pEndT;
		bikeid=pbikeId;
		tripduration=ptripDuration;
		from_station_id=fmsid;
		from_station_name=fmsName;
		to_station_id=tosId;
		to_station_name=tosName;
		usertype=pusertype;
		gender=pgender;
		birthyear=pBirthYear;
	}
	
	public String darBirthYear()
	{
		return birthyear;
	}
	public String darGender()
	{
		return gender;
	}
	public String darUsertype()
	{
		return usertype;
	}
	public int darToStationId()
	{
		return to_station_id;
	}
	public int darFromStationId()
	{
		return from_station_id;
	}
	public int darBikeId()
	{
		return bikeid;
	}
	public String darEndTIme()
	{
		return end_time;
	}
	public String darStartTime()
	{
		return start_time;
	}
	
	/**
	 * @return id - Trip_id
	 */
	public int id() {
		// TODO Auto-generated method stub
		return trip_id;
	}	
	
	
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public int getTripSeconds() {
		// TODO Auto-generated method stub
		return tripduration;
	}

	/**
	 * @return station_name - Origin Station Name .
	 */
	public String getFromStation() {
		// TODO Auto-generated method stub
		return from_station_name;
	}
	
	/**
	 * @return station_name - Destination Station Name .
	 */
	public String getToStation() {
		// TODO Auto-generated method stub
		return to_station_name;
	}

	@Override
	public int compareTo(VOTrip o) {
		int este = this.darBikeId();
		int otro = o.darBikeId();
		if(este > otro)
			return 1;
		
		if(este < otro)
			return -1;
		
		else return 0;
	}
}
