package model.vo;

/**
 * Representation of a byke object
 */
public class VOBike {
	private int id;
	private String name;
	private String city;
	private double latitude;
	private double longitude;
	private int dpcapacity;
	private String online_date;
	public VOBike(int pId, String pName, String pCity, double pLatitude, double pLongitude, int pDpCapacity, String pOnlineDate)
	{
		id=pId;
		name=pName;
		city=pCity;
		latitude=pLatitude;
		longitude=pLongitude;
		dpcapacity=pDpCapacity;
		online_date=pOnlineDate;
	}


	public String darOnlineDate()
	{
		return online_date;
	}
	/**
	 * @return id_bike - Bike_id
	 */
	public int id() {
		// TODO Auto-generated method stub
		return id;
	}	
	public String darName()
	{
		return name;
	}
	public String darCity()
	{
		return city;
	}
	public double darLatitude()
	{
		return latitude;
	}
	public double darLongitude()
	{
		return longitude;
	}
	public int darDpCapacity()
	{
		return dpcapacity;
	}
}
