package model.data_structures;

public class Nodo<T> {
	
    public Nodo<T> siguiente;
    public T dato;
    
    public Nodo( T pdato) {
        siguiente = null;
        dato = pdato;
    }
    public void cambiarSiguiente(Nodo<T> pSiguiente)
	{
		siguiente=pSiguiente;
	}
    public T darElemento()
	{
		return dato;
	}
    
}


