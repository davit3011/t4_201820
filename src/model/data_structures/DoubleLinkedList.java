package model.data_structures;

import java.util.ArrayList;
import java.util.Iterator;



public class DoubleLinkedList<T> implements IDoublyLinkedList<T>{
	
    private int numeroItems;
    private Nodo<T> raiz;
    private Nodo<T> ultimo;
    private Nodo<T> anterior;
    private Nodo<T> siguiente;
    private ArrayList<Nodo> lista;
    
    
    public DoubleLinkedList()
    {
    	raiz=null;
    	ultimo=null;
    	numeroItems=0;
    }
    
    public void addAtFirst(T element)
    {
    	Nodo <T> cabeza = new Nodo(element);
    	if(lista== null)
    	{
    		
    	}
    	
    }
    public void append(T valor) {
     if(raiz==null) {
			
			raiz=new Nodo<T>(valor);
			ultimo=raiz;
		}
		else
		{
			Nodo<T> nuevo= new Nodo<T>(valor);
			nuevo.cambiarSiguiente(raiz);
			raiz=nuevo;
		}
    }
 
    
    public boolean estaVacia() {
        return numeroItems == 0;
    }
 
   
    public T obtener(int indice) {
        if (indice < 0 || indice >= numeroItems) {
            throw new IllegalArgumentException("Error");
        }
         if (indice == 0) {
            return obtenerPrimero();
        }
         if (indice == numeroItems - 1) {
            return obtenerUltimo();
        }
 
       
 
        return buscarAdelante(indice).dato;
    }
 
 
    
    public int obtenerNumeroItems() {
        return numeroItems;
    }
 

    public T obtenerPrimero() {
        if (raiz == null) {
            throw new IllegalArgumentException("No hay items en la lista.");
        }
 
        return raiz.dato;
    }
 
    
    public T obtenerUltimo() {
        if (ultimo == null) {
            throw new IllegalArgumentException("No hay items en la lista.");
        }
 
        return ultimo.dato;
    }

	@Override
	public Nodo<T> buscarAdelante(int indice) {
		 int ultimoIndice = 0;
		 
	        Nodo<T> buscado = raiz;
	 
	        while (ultimoIndice != indice) {
	            buscado = buscado.siguiente;
	            ultimoIndice++;
	        }
	 
	        return buscado;
	}
	
	
	
	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return new ListIterator(); 
	}
	public class ListIterator implements Iterator<T>
	{
	private Nodo<T> current = raiz;
	public boolean hasNext()
	{ 
		return current != null; 
	}
	public void remove() {
	}
	public T next()
	{
	T item = current.dato;
	current = current.siguiente;
	return item;
	}
}
}

