package model.data_structures;

/**
 * Abstract Data Type for a doubly-linked list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
public interface IDoublyLinkedList<T> extends Iterable<T> {

	void append(T valor);
	boolean estaVacia();
	T obtener(int indice);
	public int obtenerNumeroItems();
	public T obtenerPrimero();
	T obtenerUltimo();
	Nodo<T> buscarAdelante(int indice);
	public void addAtFirst(T element);
	
}
