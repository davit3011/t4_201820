package model.data_structures;

import model.data_structures.DoubleLinkedList.ListIterator;

public class Queue<T> implements IQueue<T>{
	
	private Nodo<T> primero;
	private Nodo<T> ultimo;
	private int size = 0;
	
	
	public ListIterator iterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isEmpty() {
		return (primero ==null);
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return size;
	}

	@Override
	public void enqueue(T t) {
		Nodo<T> nuevo = new Nodo<T>(t);
		if(size ==0)
		{
			primero = nuevo;
			ultimo = nuevo;
		}
		else 
		{
			Nodo<T> penultimo = ultimo;
			penultimo.cambiarSiguiente(nuevo);
			ultimo = nuevo;
		}
		
		
	}

	@Override
	public T dequeue() {
		if (size == 0)
		{
			return null;
		}
		Nodo <T> viejo = primero;
		T element = primero.darElemento();
		primero = viejo.siguiente;
		viejo.cambiarSiguiente(null);
		size --;
		return element;
	}

	@Override
	public Nodo<T> darUltimo() {
		// TODO Auto-generated method stub
		return ultimo;
	}

	@Override
	public Nodo<T> darPrimero() {
		// TODO Auto-generated method stub
		return primero;
	}
}
