package model.data_structures;

import java.util.EmptyStackException;
import java.util.Iterator;

import javax.xml.soap.Node;

public class Stack<T> implements IStack<T>
{
	private Nodo<T> topePila;
	private int size =0;
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return new Iterator<T>() 
		{
			Nodo<T> act= darPrimero();
			public boolean hasNext()
			{
				if (size == 0)
				return false;
				if (act == null)
					return true;
				return act.siguiente != null;
			}
			public T next()
			{
				if (act == null)
					act = topePila;
				else
					act = act.siguiente;
				return act.darElemento();

			}
		};
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return (topePila == null);
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return size;
	}

	@Override
	public void push(T elem) {
		// TODO Auto-generated method stub
		Nodo <T> newNode = new Nodo<> (elem); 
		if (topePila == null )
		{
			topePila = newNode; 
		}
		else 
		{
			newNode.cambiarSiguiente(topePila); 
			topePila = newNode;
		}
		size++;
	}

	@Override
	public T pop(){
		// TODO Auto-generated method stub
		if (topePila == null )
			throw new EmptyStackException();
			T elem = topePila.darElemento();
			
			Nodo <T> nexTop = topePila.siguiente; 
			topePila.cambiarSiguiente(null);
			topePila = nexTop;
			size--;
			return elem;
	}
	
	public Nodo<T> darPrimero()
	{
		return topePila;
	}

	
}


