package controller;

import api.IDivvyTripsManager;
import model.data_structures.IDoublyLinkedList;
import model.logic.DivvyTripsManager;
import model.vo.VOTrip;

public class Controller {

	/**
	 * Reference to the services manager
	 */
	@SuppressWarnings("unused")
	private static IDivvyTripsManager  manager = new DivvyTripsManager();


	public static void loadTrips() {
		manager.loadTrips("./data/Divvy_Trips_2017_Q3.csv");

	}

	public static String getLastNTrips(int n){
		try{
			String respuesta = manager.getLastNTrips(n);
			return respuesta;
		}
		catch(Exception e)
		{
			return "Error al cargar los datos";
		}
	}

	public static void ordenarMerge()
	{
		manager.ordenarMerge();
	}
	public static void ordenarQuickSort()
	{
		manager.ordenarQuick();
	}
}
